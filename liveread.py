from discord.ext import commands
# Gonna copy and adjust some stuff in order to add a liveread function, as requested for the new channel
import random
import smiley_emoji

class Liveread(commands.Cog):
    @commands.command()
    async def liveread(self, ctx, *args):
        '''Join or leave liveread channels.
        Usage: %liveread <enter/exit/rules> <worm/ward/pact/pale/twigverseplus> <I have read the rules>
               i.e. %liveread enter Worm I have read the rules
        '''
        entering = None
        game = None
        check = False

        if len(args) == 0:
            await ctx.send(
                "Error: Improper format. Provide  %liveread <enter/exit/rules> <Wormverse/Otherverse/Twigverse+/all> <I have read the rules>")
            return

        if args[0] == 'enter':
            entering = True
        elif args[0] == 'exit':
            entering = False
            check = True
        elif args[0] == 'rules':
            await ctx.send(
                "Here you go: https://docs.google.com/document/d/1wl_Wl_wk884MNzeBkfqOKgmj8BaGPsVpfou13pReBEg/edit?usp=sharing")
            return
        else:
            await ctx.send(
                "Error: Improper format. Only current liveread commands are enter, exit, or rules, i.e. %liveread exit Wormverse")
            return

        if len(args) == 1:
            await ctx.send(
                "Error: Improper format. Provide  %liveread <enter/exit/rules> <Wormverse/Otherverse/Twigverse+/all> <I have read the rules>")
            return

        if len(args) > 2:
            if args[2].lower() == "i":
                if args[3].lower() == "have":
                    if args[4].lower() == "read":
                        if args[5].lower() == "the":
                            if args[6].lower() == "rules":
                                check = True

        if check == False:
            await ctx.send("Error: please read the rules of the liveread channel.")
            await ctx.send(
                "They can be found here: https://docs.google.com/document/d/1wl_Wl_wk884MNzeBkfqOKgmj8BaGPsVpfou13pReBEg/edit?usp=sharing")
            return

        valid_channels = 'worm', 'ward', 'pact', 'pale', 'twigverseplus'
        valid_channels_msg = f'Error: liveread category not found. Use \'all\' or one of the following: {", ".join(valid_channels)}'

        target_chan = args[1].lower()
        if target_chan in valid_channels:
            for channel in ctx.guild.channels:
                if channel.name == f'{target_chan}-livereads' and channel.category.name.lower() == 'serial discussion':
                    if entering:
                        await channel.set_permissions(ctx.author, read_messages=True)
                        break
                    else:
                        await channel.set_permissions(ctx.author, read_messages=False)
                        break
        elif target_chan == 'all':
            for channel in ctx.guild.channels:
                valid_channel_names = [f'{chan}-livereads' for chan in valid_channels]
                if channel.name in valid_channel_names and channel.category.name.lower() == 'serial discussion':
                    if entering:
                        await channel.set_permissions(ctx.author, read_messages=True)
                    else:
                        await channel.set_permissions(ctx.author, read_messages=False)
        else:
            await ctx.send(valid_channels_msg)
            return


    @commands.command()
    async def buffer(self, ctx, *args):
        '''Print a long message to prevent people from seeing spoilers.
        Only works in liveread channels (see %liveread rules).
        Usage: %buffer <Chapter>
            ex. %buffer Ward 5.2
                %buffer Pale One After Another, interlude D'''
        if not 'liveread' in ctx.channel.name:
            await ctx.send('Error: I can only buffer liveread channels!')
            return
        emoji = smiley_emoji.random_emoji()
        msg = (emoji + '\n') * 60 + ' '.join(args)
        await ctx.send(msg)
