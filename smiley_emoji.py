import re, random

def random_emoji():
    with open('emoji.txt') as emoji_file:
        lines = emoji_file.readlines()
    data_lines = [l for l in lines if len(l.strip()) != 0 and l.strip()[0] != '#']
    emojis = []
    for l in data_lines:
        emoji = re.search(r'#\s(.*?)\s',l).group(1)
        if emoji is not None:
            emojis.append(emoji)
    return random.choice(emojis)
