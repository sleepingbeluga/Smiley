from discord.ext import commands
import discord, json

class Town_Hall(commands.Cog):
    # Gonna do my best here
    def __init__(self, b):
        self.b = b
        self.chan = 874401694287536238
        self.open = True
        try:
            with open('blacklist.json', 'r+') as bl:
                self.blacklist = json.load(bl)
        except FileNotFoundError:
            with open('blacklist.json', 'w+') as bl:
                json.dump([], bl)
                self.blacklist = []

    @commands.command()
    async def openhall(self, ctx, *args):
        '''Mod command only. Opens the town hall for communication in a given channel.
        '''

        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and ('Author' not in (str(role) for role in ctx.author.roles)) and ('Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send("Only moderators may use this command.")
            return

        if self.open == True:
            await ctx.send("The town hall is open elsewhere.")
            return

        self.open = True
        self.chan = ctx.channel.id
        await ctx.send("Town hall now open.")

    @commands.command()
    async def closehall(self, ctx, *args):
        '''Mod command only. Closes the town hall for communication in a given channel.
        '''

        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and ('Author' not in (str(role) for role in ctx.author.roles)) and ('Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send("Only moderators may use this command.")
            return

        if self.open == False:
            await ctx.send("The town hall is not open.")
            return

        if ctx.channel.id != self.chan:
            await ctx.send("The town hall is open elsewhere.")
            return

        self.open = False
        self.chan = None
        await ctx.send("Town hall now closed.")

    @commands.command()
    async def report(self, ctx, *args):
        '''Send a message straight to the mods.
        '''

        if self.open == False:
            await ctx.send("Sorry, the town hall has not been opened by the moderators.")
            return

        if str(ctx.author.id) in self.blacklist:
            await ctx.send("Sorry, you are not allowed to send messages to the town hall.")
            return

        message = ''
        for arg in args:
            message += arg + ' '

        channel = self.b.get_channel(self.chan)
        await channel.send("<@!" + str(ctx.author.id) + '>:\n' + message)

    @commands.command()
    async def blacklist(self, ctx, *args):
        '''Mod command only. Blocks a user from sending messages to the town hall.
        '''

        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and (
                'Author' not in (str(role) for role in ctx.author.roles)) and (
                'Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send("Only moderators may use this command.")
            return

        if args[0] == 'show':
            message = 'Current names in blacklist:\n'
            for name in self.blacklist:
                message += "<@!" + name + '>\n'
            await ctx.send(message)
            return

        elif args[0] == 'remove':
            if args[1][3:-1] in self.blacklist:
                self.blacklist.remove(args[1][3:-1])
                await ctx.send("Removed " + str(args[1]) + " from blacklist.")
            else:
                await ctx.send(str(args[1]) + " not found on blacklist.")
                return

        else:
            if args[0] in self.blacklist:
                await ctx.send(str(args[0]) + " is already on the blacklist.")
                return
            self.blacklist.append(str(args[0])[3:-1])
            await ctx.send("Added " + str(args[0]) + " to blacklist.")

        with open('blacklist.json', 'w+') as bl:
            json.dump(self.blacklist, bl)